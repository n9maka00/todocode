<h3><?= $title ?></h3>
<form action="/login/registration">
<div class="col-12">
<?= \config\Services::validation()->listErrors();?>
</div>
<div class="form-group">
<label>Username</label>
<input class="form-control" name="user" placeholder="Enter username" maxlenght="30">
</div>
<div class="form-group">
<label>First name</label>
<input class="form-control" name="fname" placeholder="Enter first name" maxlenght="100">
</div>
<div class="form-group">
<label>Last name</label>
<input class="form-control" name="lname" placeholder="Enter last name" maxlenght="100">
</div>
<div class="form-group">
<label>Password</label>
<input class="form-control" name="password" type="password" placeholder="Enter password" maxlenght="255">
</div>
<div class="form-group">
<label>Password again</label>
<input class="form-control" name="confirmpassword" type="password" placeholder="Enter password again" maxlenght="255">
</div>
<button class="btn btn-primary">Submit</button>
</form>